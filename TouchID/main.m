//
//  main.m
//  TouchID
//
//  Created by Omer Aktuna on 9/24/14.
//  Copyright (c) 2014 Markafoni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
