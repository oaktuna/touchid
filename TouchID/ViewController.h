//
//  ViewController.h
//  TouchID
//
//  Created by Omer Aktuna on 9/24/14.
//  Copyright (c) 2014 Markafoni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnAuthenticate;
- (IBAction)btnAuthenticateTapped:(id)sender;

@end

