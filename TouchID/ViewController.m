//
//  ViewController.m
//  TouchID
//
//  Created by Omer Aktuna on 9/24/14.
//  Copyright (c) 2014 Markafoni. All rights reserved.
//

#import "ViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnAuthenticateTapped:(id)sender {
    LAContext *context = [[LAContext alloc] init];
    
    NSError *error = nil;
    
    // Check if the device can evaluate the policy.
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        // Authenticate User
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:@"Are you the device owner?"
                          reply:^(BOOL success, NSError *error) {
                              if (success) {
                                  [self presentAlertControllerWithMessage:@"You are the device owner!" titleRef:@"Success"];
                                  
                              }
                              else {
                                  [self handleTouchIdErrorTypes:error];
                              }
        }];
    }
    else {
        [self handleTouchIdErrorTypes:error];
    }
}

- (void)handleTouchIdErrorTypes:(NSError *)error {
    if (error.code == LAErrorAuthenticationFailed) { //Yanlıs parmak izi (3. kere yanlıs girdiginde)
        [self presentAlertControllerWithMessage:@"Authentication Failed!" titleRef:@"Error"];
        return;
    }
    else if (error.code == LAErrorPasscodeNotSet) {
        [self presentAlertControllerWithMessage:@"Passcode not set!" titleRef:@"Error"]; //passcode(parola) belirlenmemis
        return;
    }
    else if (error.code == LAErrorTouchIDNotAvailable) {
        [self presentAlertControllerWithMessage:@"Touch ID not available!" titleRef:@"Error"]; //telefon touch id desteklemiyor.
        return;
    }
    else if (error.code == LAErrorTouchIDNotEnrolled) {
        [self presentAlertControllerWithMessage:@"Touch ID kayıtlı değil!" titleRef:@"Error"]; //parmak izi kayıtlı degil
        return;
    }
    else if (error.code == LAErrorSystemCancel) {
        [self presentAlertControllerWithMessage:@"Touch ID system canceled!" titleRef:@"Error"];
        return;
    }
    else if (error.code == LAErrorUserCancel) {
        [self presentAlertControllerWithMessage:@"Touch ID user canceled!" titleRef:@"Error"]; //Vazgeç
        return;
    }
    else if (error.code == LAErrorUserFallback) {
        [self presentAlertControllerWithMessage:@"User request to enter passcode!" titleRef:@"Error"]; //Passcode
        return;
    }
}

- (void)presentAlertControllerWithMessage:(NSString *) message titleRef:(NSString *)title {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
